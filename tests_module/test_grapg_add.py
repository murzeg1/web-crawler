from rdflib import Graph, URIRef, RDF, RDFS, Literal

g = Graph()

test = URIRef("http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#ТестТТ")
testName = URIRef("http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Тест")
person = URIRef("http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
label = URIRef("'http://www.w3.org/2000/01/rdf-schema#label'")

g.add((test, RDF.type, person))
g.add((test, RDF.type, owlnamed))
g.add((test, RDFS.label, Literal("Тест Т.Т.", lang='ru')))

print(g.serialize(destination="test_ontology.ttl", format="turtle"))

