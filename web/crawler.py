from html.parser import HTMLParser
from urllib.request import urlopen
from urllib import parse
import pandas as pd

FILENAME = "sitetocsv.txt"


class LinkParser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for (key, value) in attrs:
                if key == 'href':
                    newUrl = parse.urljoin(self.baseUrl, value)
                    self.links = self.links + [newUrl]


    def getLinks(self, url):
        self.links = []
        self.baseUrl = url
        response = urlopen(url)
        contType = response.getheader('Content-Type')
        if 'text/html' in contType:
            htmlBytes = response.read()
            htmlString = htmlBytes.decode("utf-8")
            self.feed(htmlString)

            return htmlString, self.links
        else:
            return "",[]


def spider(url, maxPages, name):
    mainUrl = url
    pagesToVisit = [url]
    visitedPages =[]
    numberVisited = 0
    while numberVisited < maxPages and pagesToVisit != [] :

        url = pagesToVisit[0]
        pagesToVisit = pagesToVisit[1:]

        if mainUrl in url and url not in visitedPages:
            numberVisited = numberVisited + 1
            name = getTable(url, name)
            visitedPages.append(url)
            try:
                print(numberVisited, "Visiting:", url)
                parser = LinkParser()
                data, links = parser.getLinks(url)
                pagesToVisit = pagesToVisit + links
            except:
                print("Fail")
    return name


def spiderWrapper(l, maxPages):
    name = 0
    if len(l) != 0:
        for i in l:
            name = spider(i, maxPages, name)
    else:
        print("URL list is empty")


def getTable(url, name):
    try:
        df_list = pd.read_html(url)
        counter = len(df_list)
        print("table num:", counter)
        while counter > 0:
            # print(df_list[counter-1].columns)
            # print(df_list[counter-1].shape[0] + df_list[counter-1].shape[1])
            if(df_list[counter-1].shape[0] + df_list[counter-1].shape[1]) > 5:
                print("FOUND TABLE")
                filename = str(name) + '.csv'
                print("filename: ", filename)
                if isinstance(name, int):
                    name+=1
                df_list[counter-1].to_csv(filename)
                f = open(FILENAME, 'a')
                f.write(str(url) + " " + filename + "\n")
                f.close
            counter-=1
    except:
        print("No tables")
    return name


f = open(FILENAME, 'w')
spider("https://www.iis.nsk.su/", 25, 0)
# spiderWrapper(["http://www.ict.nsc.ru", "https://www.iis.nsk.su/"], 25)
# spiderWrapper([], 25)