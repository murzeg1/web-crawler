from flask import Flask, render_template, flash, url_for, redirect
from web.forms import LoginForm, CrawlForm, ShowPresetForm
from web import parser

app = Flask(__name__)

app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))


@app.route("/")
@app.route('/index')
def index():
    flash("Web crawler. Searching for web-tables. Extracting into CSV files. Displaying as html tables.")
    return render_template('base.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html',  title='Sign In', form=form)


@app.route('/crawl', methods=['GET', 'POST'])
def crawl():
    form = CrawlForm()
    if form.validate_on_submit():
        flash("Requested Crawl for Url {}".format(form.url.data))
        print(form.url.data)
        df = parser.getTableReturnHTML(form.url.data)
        flash("crawl done")
        return render_template('crawl.html', title='Url', form=form, tables=df)
    return render_template('crawl.html', title='Url', form=form, tables = [])


@app.route('/showpreset', methods=['GET', 'POST'])
def showpreset():
    form = ShowPresetForm()
    if form.validate_on_submit():
        flash("Requested Crawl for Url {}".format(form.url.data))
        return render_template('showpreset.html', title='Url', form=form, tables=parser.datalist)
    return render_template('showpreset.html', title='Url', form=form, tables=[])


if __name__ == "__main__":
    app.run()