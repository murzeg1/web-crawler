from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, URL


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class CrawlForm(FlaskForm):
    url = StringField("Url", validators=[DataRequired(), URL()])
    submit = SubmitField('Crawl It')

class ShowPresetForm(FlaskForm):
    url = StringField("Url")
    submit = SubmitField('show preset from url list')