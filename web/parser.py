from html.parser import HTMLParser
from urllib.request import urlopen
from urllib import parse
import pandas as pd
import glob


FILENAME = "TESTurltocsv.txt"
validwords = ["Конференция", "Конференции", "conference", "Conference", "Сотрудники", "Persons", "сотрудники", "persons"]

class LinkParser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for (key, value) in attrs:
                if key == 'href':
                    newUrl = parse.urljoin(self.baseUrl, value)
                    self.links = self.links + [newUrl]


    def getLinks(self, url):
        self.links = []
        self.baseUrl = url
        response = urlopen(url)
        contType = response.getheader('Content-Type')
        if 'text/html' in contType:
            htmlBytes = response.read()
            htmlString = htmlBytes.decode("utf-8")
            self.feed(htmlString)

            return htmlString, self.links
        else:
            return "",[]

def checkForWord(words, url):
    try:
        parser = LinkParser()
        data, links = parser.getLinks(url)
    except:
        print("Fail to load: ", url)
        return 0

    for i in words:
        if data.find(i)>-1:
            print("Word ", i, " Found in ", url)
            return 1
    print("Words not found")
    return 0


def generateCsvName(url, counter):
    url = url.replace("/", "")
    url = url.replace(":", "")
    url = url.replace("httpwww.", "")
    return "output2/" + str(counter) + str(url) + ".csv"


def prefiltration(df):
    count_nan = len(df) - df.count()
    if df.shape[0] * df.shape[1] > count_nan.sum():
        print("prefilt success")
        return 1
    else:
        print("prefilt fail")
        return 0



def readUrls():
    df = pd.read_csv("data/urls.csv")
    df = pd.read_csv("data/urlsisc.csv")
    print(df)
    print(list(df.columns.values))
    url_list = df['url'].tolist()
    print(url_list)
    return url_list


def getTablesFromUrlList(urls):
    for i in urls:
        if checkForWord(validwords, i) == 1:
            getTable(i)


def loadCsv(folder):
    path = str(folder) + "/*.csv"
    datalist = []
    for fname in glob.glob(path):
        dataf = pd.read_csv(fname)
        datalist.append(dataf)
    return datalist


def allDfToHtml(dataf):
    newdf = []
    for i in dataf:
        if isinstance(i, pd.DataFrame):
            dfhtml = i.to_html(classes="table table-striped")
            newdf.append(dfhtml)
    return newdf


def prefiltrationDim(df):
    if isinstance(df, pd.DataFrame):
        if((df.shape[0] > 2 and df.shape[1] < 10) and (df.shape[0] + df.shape[1] > 15)):
            return 1
    else:
        return 0


def getTable(url):
    try:
        df_list = pd.read_html(url)
        counter = len(df_list)
        print("table num:", counter)
        while counter > 0:
            if prefiltrationDim(df_list[counter - 1]):
                print("FOUND TABLE")
                prefiltration(df_list[counter-1])
                filename = generateCsvName(url, counter)
                print("filename: ", filename)
                df_list[counter-1].to_csv(filename)
                f = open(FILENAME, 'a')
                f.write(str(url) + " " + filename + "\n")
                f.close
            counter-=1
    except:
        print("No tables")


def justTestModule():
    print("test test")
    return



def getTableReturnHTML(url):
    try:
        dfforwebwithhtml = []
        df_list = pd.read_html(url)
        counter = len(df_list)
        print("table num:", counter)
        while counter > 0:
            if prefiltrationDim(df_list[counter - 1]):
                print("FOUND TABLE")
                prefiltration(df_list[counter-1])
                filename = generateCsvName(url, counter)
                print("filename: ", filename)
                dfforwebwithhtml.append(df_list[counter - 1])
            counter-=1
        if len(dfforwebwithhtml) != 0:
            dfforwebwithhtml = allDfToHtml(dfforwebwithhtml)
            return dfforwebwithhtml
        else:
            return ["NO TABLES"]
    except:
        print("No tables")


def spider(url, maxPages):
    mainUrl = url
    pagesToVisit = [url]
    visitedPages =[]
    numberVisited = 0
    while numberVisited < maxPages and pagesToVisit != [] :
        url = pagesToVisit[0]
        pagesToVisit = pagesToVisit[1:]
        if mainUrl in url and url not in visitedPages:
            numberVisited = numberVisited + 1
            if checkForWord(validwords, url) == 1:
                getTable(url)
                visitedPages.append(url)
                try:
                    print(numberVisited, "Visiting:", url)
                    parser = LinkParser()
                    data, links = parser.getLinks(url)
                    pagesToVisit = pagesToVisit + links
                except:
                    print("Fail")


def spiderWrapper(l, maxPages):
    if len(l) != 0:
        for i in l:
            spider(i, maxPages)
    else:
        print("URL list is empty")


# urls = readUrls()

# df = pd.read_csv("wow.csv")
# df2 = pd.read_csv("wow2.csv")

datalist = loadCsv("output2")
datalist = allDfToHtml(datalist)

# print(datalist)
# getTablesFromUrlList(urls)
# f = open(FILENAME, 'w')

# prefiltration()
# getTable("http://www.ict.nsc.ru/ru/structure/persons")
# getTable("http://www.pythonicway.com/python-operators")

# pd.read_html("http://www.ict.nsc.ru/ru/structure/persons")

# spiderWrapper(urls, 20)

