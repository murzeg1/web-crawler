import gensim


def initModel():
    w2v_fpath = "../w2cmodels/all.norm-sz100-w10-cb0-it1-min100.w2v"
    w2v = gensim.models.KeyedVectors.load_word2vec_format(w2v_fpath, binary=True, unicode_errors='ignore')
    w2v.init_sims(replace=True)
    w2v.init_sims()
    return w2v

def getWordVector(word, model):
    if word in model:
        return model[word]
    else:
        print("no such word in model")

def getWordSimilar(word, model):
    if word in model:
        for word, score in model.most_similar(word):
            return word, score
    else:
        print("there is no such word in model")

def checkSimilarityTwoWords(word1, word2, model):
    if word1 in model and word2 in model:
        print(model.similarity(word1, word2))
        return model.similarity(word1, word2)
    else:
        print("impossible to check similarity")
        return 0

def checkSimilarityTwoLists(list1, list2, model):
    for i in list1:
        if i not in model:
            print("impossible to compute cos, no word ", i, " in model")
            return None
    for i in list2:
        if i not in model:
            print("impossible to compute cos, no word ", i, " in model")
            return None
    return model.n_similarity(list1, list2)

# model = initModel()
# print(checkSimilarityTwoLists([u"человек", u"король"], [u"персона", u"королева"], model))
