import requests
apikey = "dict.1.1.20190327T170756Z.7162a115163749d5.041837359d96f0a978698726c48a4f3c98b65ad1"
text = "Персона"
lang = "ru-ru"



def getSynonimsList(str):
    """
    return list with synonims to string
    :param str: string for api
    :return: list with synonims
    """
    dict_params = {'key': apikey, 'text': text, 'lang': lang}
    dict_params['text'] = str

    # r = requests.get("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20190327T170756Z.7162a115163749d5.041837359d96f0a978698726c48a4f3c98b65ad1&lang=en-ru&text=time")
    r = requests.get("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?", params=dict_params)

    synonyms = []

    print(r.json())
    if r.json().get('def') == []:
        return []
    syn_list = r.json().get('def')[0].get('tr')
    print(syn_list)

    for tr in syn_list:
        if tr is not None:
            synonyms.append(tr.get('text'))
            print(tr.get('syn'))
            if tr.get('syn') is not None:
                for dic in tr.get('syn'):
                    if isinstance(dic, dict):
                        synonyms.append(dic.get("text"))

    print(synonyms)
    return synonyms

# getSynonimsList("Звание")