import pandas as pd
import glob
# from ontology.ontology_controller import DEGREE_POOL_FROM_ONTOLOGY, PHD_POOL_FROM_ONTOLOGY, POST_POOL_FROM_ONTOLOGY

FIO_POOL = ["Ф.И.О.", "Фамилия Имя Отчество", "ФИО", "Имя"]
POST_POOL = ["Должность", "Пост", "Работа"]
DEGREE_POOL = ["Степень", "Ученая степень", "звание"]





folder = "../csv_for_mappers"

def loadCsv(folder=folder):
    """
    load csv with web tables
    :param folder:
    :return: list with dataframes
    """
    path = str(folder) + "/*.csv"
    datalist = []
    for fname in glob.glob(path):
        dataf = pd.read_csv(fname)
        datalist.append(dataf)
    return datalist

df = loadCsv(folder)

df_test = df[0]

print(df_test)

def mapping(df):
    """
    mapping by same word rule
    return degree post and fio list from dataframe
    :param df:
    :return: degree list post list fio list
    """
    counter = 0
    if isinstance(df, pd.DataFrame) != True:
        return [], [], []
    print(len(df.index))
    for index, row in df.iterrows():
         for i in row:
             counter+=1
             print(i)
             print(type(i))
             if isinstance(i, str):
                 print("this is string")
                 for fio_value in FIO_POOL:
                     if fio_value.lower() in i.lower():
                        columnFIO = counter%len(df.index)
                 for post_value in POST_POOL:
                     if post_value.lower() in i.lower():
                        columnPOST = counter%len(df.index)
                 for degree_value in DEGREE_POOL:
                     if degree_value.lower() in i.lower():
                        columnDEGREE = counter%len(df.index)

    print(counter)
    print("column FIO: ", columnFIO)
    print("column POST: ", columnPOST)
    print("column DEGREE: ", columnDEGREE)

    listoflists = df.iloc[:,columnFIO-1:columnFIO].values.tolist()
    fio_l = []
    for i in listoflists:
        fio_l.append(i[0])
    print(type(fio_l))
    print(fio_l)

    for i in fio_l:
        for fio_value in FIO_POOL:
            if isinstance(i, str):
                if fio_value.lower() in i.lower():
                    fio_l.remove(i)
                    break
    print(fio_l)

    listoflists = df.iloc[:,columnPOST-1:columnPOST].values.tolist()
    post_l = []
    for i in listoflists:
        post_l.append(i[0])
    print(type(post_l))
    print(post_l)

    for i in post_l:
        for post_value in POST_POOL:
            if isinstance(i, str):
                if post_value.lower() in i.lower():
                    post_l.remove(i)
                    break
    print(post_l)

    listoflists = df.iloc[:,columnDEGREE-1:columnDEGREE].values.tolist()
    degree_l = []
    for i in listoflists:
        degree_l.append(i[0])
    print(type(degree_l))
    print(degree_l)

    for i in degree_l:
        for degree_value in DEGREE_POOL:
            if isinstance(i, str):
                if degree_value.lower() in i.lower():
                    degree_l.remove(i)
                    break
    print(degree_l)

    print(len(degree_l), len(post_l), len(fio_l))
    return degree_l, post_l, fio_l

# for i in df:
#     mapping(i)
