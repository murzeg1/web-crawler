from rdflib import Graph

def getPersonLastNames(g):
    '''
    SPARQL query to ontology to get all lastnames form enity Person
    :param g:
    :return: dict with all lastnames
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?lastname 
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Фамилия> ?lastname .
    }
    """

    q = g.query(sparql)

    lastNameList = {}

    for row in q:
        lastNameList[row[0].n3()] = row[1].toPython()

    print(lastNameList)
    return lastNameList


def getPersonNames(g):
    '''
    SPARQL query to ontology to get all names from entity Person
    :param g:
    :return: dict with person names
    '''

    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?name 
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Имя> ?name .
    }
    """

    q = g.query(sparql)

    nameList = {}

    for row in q:
        nameList[row[0].n3()] = row[1].toPython()

    print(nameList)
    return nameList


def getPersonLastName(g, person):
    """
    get person last name by person uri
    :param g:
    :param person:
    :return str with last name:
    """
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?name 
    Where {
    <""" + person +"""> a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    <""" + person +"""> <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Фамилия> ?name .
    }
    """

    q = g.query(sparql)
    for row in q:
        print(row[0].toPython())
    return row[0].toPython()

def getPersonName(g, person):
    """
    get person name by person uri
    :param g:
    :param person:
    :return str with name:
    """
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?name 
    Where {
    <""" + person +"""> a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    <""" + person +"""> <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Имя> ?name .
    }
    """

    q = g.query(sparql)
    for row in q:
        print(row[0].toPython())
    return row[0].toPython()


def getPersonMiddleName(g, person):
    """
    get person middle name by person uri
    :param g:
    :param person:
    :return str with middle name:
    """
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?name 
    Where {
    <""" + person +"""> a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    <""" + person +"""> <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Отчество> ?name .
    }
    """

    q = g.query(sparql)
    for row in q:
        print(row[0].toPython())
    return row[0].toPython()


def getPersonInititals(g, person):
    """
    get person initials by person uri
    :param g:
    :param person:
    :return str with initials:
    """
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?name 
    Where {
    <""" + person +"""> a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    <""" + person +"""> <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Инициалы> ?name .
    }
    """

    q = g.query(sparql)
    for row in q:
        print(row[0].toPython())
    return row[0].toPython()


def getPersonMiddleNames(g):
    '''
    SPARQL query to ontology to get all middlenames form enity Person
    :param g:
    :return: dict with all middlenames
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?lastname 
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Отчество> ?lastname .
    }
    """

    q = g.query(sparql)

    middleNameList = {}

    for row in q:
        # middleNameList[row[0].n3()] = row[1].n3().split("@")[0]
        middleNameList[row[0].n3()] = row[1].toPython()

    print(middleNameList)
    return middleNameList


def getPersonBirthDates(g):
    '''
    SPARQL query to ontology to get all birthdates form enity Person
    :param g:
    :return: dict with all birthdates
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?lastname 
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_ДатаРождения> ?lastname .
    }
    """

    q = g.query(sparql)

    birthdatesList = {}

    for row in q:
        # birthdatesList[row[0].n3()] = row[1].n3().split("@")[0]
        birthdatesList[row[0].n3()] = row[1].toPython()

    print(birthdatesList)
    return birthdatesList


def getPersonWorkPlaces(g):
    '''
    SPARQL query to ontology to get all workplaces form enity Person
    :param g:
    :return: dict with all workplaces
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?lastname ?workplace
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Персона> ?lastname .
    ?lastname <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Организация> ?workplace
    }
    """

    q = g.query(sparql)

    birthdatesList = {}

    for row in q:
        # birthdatesList[row[0].n3()] = row[1].n3().split("@")[0]
        birthdatesList[row[0].n3()] = row[2].toPython()

    print(birthdatesList)
    return birthdatesList


def getPersonPosts(g):
    '''
    SPARQL query to ontology to get all posts form enity Person
    :param g:
    :return: dict with all posts
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?lastname ?post
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Персона> ?lastname .
    ?lastname <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Должность> ?post
    }
    """

    q = g.query(sparql)

    birthdatesList = {}

    for row in q:
        # birthdatesList[row[0].n3()] = row[1].n3().split("@")[0]
        birthdatesList[row[0].n3()] = row[2].toPython()

    print(birthdatesList)
    return birthdatesList


def getPersonPhD(g):
    '''
    SPARQL query to ontology to get all phd form enity Person
    :param g:
    :return: dict with all posts
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?phd 
    Where {
    ?all a <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона>, owl:NamedIndividual .
    ?all <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_УченаяСтепень> ?phd .
    }
    """

    q = g.query(sparql)

    phdList = {}

    for row in q:
        # birthdatesList[row[0].n3()] = row[1].n3().split("@")[0]
        phdList[row[0].n3()] = row[1].toPython()

    print(phdList)
    return phdList


def getAllPhDValues(g):
    '''
    SPARQL query to ontology to get all PhD values from ontology
    :param g:
    :return: list with all possible PhD values
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?phd 
    Where {
    ?phd  a  owl:NamedIndividual, <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#УченаяСтепень> .
    }
    """

    q = g.query(sparql)

    phdList = []

    for row in q:
        phdList.append(row[1].toPython())

    print(phdList)
    return phdList


def getAllPostValues(g):
    '''
    SPARQL query to ontology to get all post values from ontology
    :param g:
    :return: list with all possible post values
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?phd 
    Where {
    ?phd  a  owl:NamedIndividual, <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Должность> .
    }
    """

    q = g.query(sparql)

    postList = []

    for row in q:
        postList.append(row[1].toPython())

    print(postList)
    return postList


def getAllDegreeValues(g):
    '''
    SPARQL query to ontology to get all degree values from ontology
    :param g:
    :return: list with all possible degree values
    '''
    sparql = """ 
    PREFIX :      <http://www.semanticweb.org/gal/ontologies/2018/0/9/DMS-ontology#> 
    PREFIX owl:   <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xml:   <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>

    Select ?all ?phd 
    Where {
    ?phd  a  owl:NamedIndividual, <http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#УченоеЗвание> .
    }
    """

    q = g.query(sparql)

    degreeList = []

    for row in q:
        degreeList.append(row[1].toPython())

    print(degreeList)
    return degreeList