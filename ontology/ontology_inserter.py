from rdflib import URIRef,BNode, Literal, RDF, Graph, RDFS, OWL
import ontology.ontology_extractor

def deleteDotsInInitials(initials):
    return initials.replace(".", "")

def insertPersonAndLabel(g, pers, label="Default"):
    """
    Insert Person and label into onlotogy
    :param g, person, person:
    :return:
    """

    personN = URIRef(pers)
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')

    g.add((personN, RDF.type, person))
    g.add((personN, RDF.type, owlnamed))
    g.add((personN, RDFS.label, Literal(label, lang='ru')))

    g.serialize(destination = "ttls/new_ontology.ttl", format="turtle")
    return


def insertPersonLastName(g, pers, personLastName):
    """
    Insert Person Last Name into onlotogy
    :param g, personName, person:
    :return:
    """

    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personLastNamePredicat = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Фамилия')
    personN = URIRef(pers)

    g.add((personN, personLastNamePredicat, Literal(personLastName, lang="ru")))

    g.serialize(destination="ttls/new_ontology.ttl", format="turtle")
    return


def insertPersonName(g, pers, personName):
    """
    Insert Person Name into onlotogy
    :param g, personName, person:
    :return:
    """

    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personNamePredicat = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Имя')
    personN = URIRef(pers)

    g.add((personN, personNamePredicat, Literal(personName, lang='ru')))

    g.serialize(destination="ttls/new_ontology.ttl", format="turtle")
    return


def insertPersonMiddleName(g, pers, personMiddleName):
    """
    Insert person middlename in ontology
    :param g:
    :param pers:
    :param personMiddleName:
    :return:
    """
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personMiddleNamePredicat = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Отчество')
    personN = URIRef(pers)

    g.add((personN, personMiddleNamePredicat, Literal(personMiddleName, lang='ru')))

    g.serialize(destination="ttls/new_ontology.ttl", format="turtle")
    return

def insertPersonInitials(g, pers, personInitials):
    """
    Insert person initials in ontology
    :param g:
    :param pers:
    :param personInitials:
    :return:
    """
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personMiddleNamePredicat = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Инициалы')
    personN = URIRef(pers)

    g.add((personN, personMiddleNamePredicat, Literal(personInitials, lang='ru')))

    g.serialize(destination="ttls/new_ontology.ttl", format="turtle")
    return


def insertPersonBirthDate(g, pers, personBirthDate):
    """
    insert person birthdate in onlotogy
    :param g:
    :param pers:
    :param personBirthDate:
    :return:
    """
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personBirthDatePredicat = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_ДатаРождения')
    personN = URIRef(pers)

    g.add((personN, personBirthDatePredicat, Literal(personBirthDate)))

    g.serialize(destination="ttls/new_ontology.ttl", format="turtle")
    return


def servicePersonWorkClassStringCreater(person, personinitials = "АА", workplace = "НЕИЗВЕСТНО"):
    """
    Creates service class name for workplaces and post
    :param person:
    :param workplace:
    :return string :
    """
    uri = "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#" + person + personinitials + "_" + workplace
    return uri


def addServicePersonWorkClass(g, serviceclassname):
    """
    add service class to ontology
    :param g:
    :param serviceclassname:
    :return:
    """
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personBirthDatePredicat = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_ДатаРождения')
    personworkpersonorg = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Персона')
    workpersorgpost = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Должность')
    workpersorg = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация')
    uri = URIRef(serviceclassname)

    g.add((uri, RDF.type, owlnamed))
    g.add((uri, RDF.type, workpersorg))

    return


def insertPersonPost(g, pers, personPost, workplace = "НЕИЗВЕСТНО"):
    """
    insert person post in onlotogy
    :param g: grapg of ontology
    :param pers: ontology person link with prefix
    :param personPost: ontology person post link with prefix
    :param workplace: str with workplace name default = НЕИЗВЕСТНО
    :return:
    """
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personworkpersonorg = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Персона')
    workpersorgpost = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Должность')
    personN = URIRef(pers)

    scsc = servicePersonWorkClassStringCreater(ontology.ontology_extractor.getPersonLastName(g,pers),deleteDotsInInitials(ontology.ontology_extractor.getPersonInititals(g, pers)) , workplace)
    uri = URIRef(scsc)
    addServicePersonWorkClass(g, scsc)

    g.add((uri, URIRef(workpersorgpost), URIRef(personPost)))
    g.add((personN, URIRef(personworkpersonorg), uri))

    return



def insertPersonWorkPlace(g, pers, personWorkPlace, workplace = "НЕИЗВЕСТНО"):
    """
    insert person workplace in ontology
    :param g: graph of ontology
    :param pers: ontology person link with prefix
    :param workplace: string with workplace name default = НЕИЗВЕСТНО
    :param personWorkPlace: ontology organization link with prefix
    :return:
    """
    person = URIRef(u"http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона")
    owlnamed = URIRef(u'http://www.w3.org/2002/07/owl#NamedIndividual')
    personworkpersonorg = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Персона')
    personworkorgorg = URIRef(u'http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#работает_Персона_Организация_Организация')
    personN = URIRef(pers)

    scsc = servicePersonWorkClassStringCreater(ontology.ontology_extractor.getPersonLastName(g,pers),deleteDotsInInitials(ontology.ontology_extractor.getPersonInititals(g, pers)) , workplace)
    uri = URIRef(scsc)
    addServicePersonWorkClass(g, scsc)

    g.add((personN, URIRef(personworkpersonorg), uri))
    g.add((uri, URIRef(personworkorgorg), URIRef(personWorkPlace)))

    return

def saveOntology(g):
    g.serialize(destination="ttls/new_ontology.ttl", format="turtle")
    return


