import ontology.ontology_inserter, ontology.ontology_extractor
from rdflib import Graph
from pathlib import Path
import mappers.map_by_same_words

def deletePrefixFromList(list):
    """
    Delete Prefix http://www.semanticweb.org/IIS/ontologies/INIR_Ontology# from all string in list
    :param list:
    :return newlist without prefix:
    """
    new_list = []
    for i in list:
        if "INIR_Ontology#" in i:
            new_list.append(i.split("INIR_Ontology#")[1])
    return new_list


def splitFIO(str):
    return str.split(" ")

def addPrefixtoStr(str):
    """
    Add Prefix http://www.semanticweb.org/IIS/ontologies/INIR_Ontology# to string
    :param str:
    :return string with prefix:
    """
    return "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#" + str


def getListForWord2VecSimiliarity(list):
    """
    Delete underscore from all strings in list
    :param list:
    :return list rdy dor word2vec similiriaty func:
    """
    new_list = []
    for i in list:
        for j in i.split("_"):
            new_list.append(j)
    return new_list


person_name = "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона_Имя"
person = "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#Персона"
sivakova = "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#СиваковаТВ"

data_folder = Path("/ttls")
file_to_open = data_folder / "ontology.ttl"

g = Graph()
# print(file_to_open)
g.load('ttls/ontology.ttl', format="turtle")

before_insert = len(g)

DEGREE_POOL_FROM_ONTOLOGY = ontology.ontology_extractor.getAllDegreeValues(g)
PHD_POOL_FROM_ONTOLOGY = ontology.ontology_extractor.getAllPhDValues(g)
POST_POOL_FROM_ONTOLOGY =  ontology.ontology_extractor.getAllPostValues(g)


nepomPerson = addPrefixtoStr("НепомнящийИА")
nepomPersonName = "Илья"
nepomPersonLastName = "Непомнящий"

degs, posts, fios = mappers.map_by_same_words.mapping(mappers.map_by_same_words.loadCsv("../csv_for_mappers")[0])


def createLastNameWithInitials(fiolist):
    return fiolist[0] + fiolist[1][0] + fiolist[2][0]

def createLastNameWithInitialsLabel(fiolist):
    return fiolist[0] + " " + fiolist[1][0]+ "." + fiolist[2][0]

def createPersonInitials(fiolist):
    return fiolist[1][0] + "." + fiolist[2][0] + "."

# for i in fios:
#     print(createLastNameWithInitialsLabel(splitFIO(i)))

def createPostWithPrefix(post):
    return "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#" + post +"_должность"

def replaceSpacesWithUndeScore(str):
    return str.replace(" ", "_")

def insertPersonInOntology(g, Fio, Post, Degree):
    splittedFIO = splitFIO(Fio)
    fiowithinitials = createLastNameWithInitials(splittedFIO)
    fiowithinitialslabel = createLastNameWithInitialsLabel(splittedFIO)
    prefixedfiowithinitials = addPrefixtoStr(fiowithinitials)
    personinitials = createPersonInitials(splittedFIO)
    personname = splittedFIO[1]
    personlastname = splittedFIO[0]
    personmiddlename = splittedFIO[2]
    ontology.ontology_inserter.insertPersonAndLabel(g, prefixedfiowithinitials, fiowithinitialslabel)
    ontology.ontology_inserter.insertPersonName(g,prefixedfiowithinitials, personname)
    ontology.ontology_inserter.insertPersonLastName(g,prefixedfiowithinitials, personlastname)
    ontology.ontology_inserter.insertPersonMiddleName(g,prefixedfiowithinitials, personmiddlename)
    ontology.ontology_inserter.insertPersonInitials(g, prefixedfiowithinitials, personinitials)


    underscoredpost = replaceSpacesWithUndeScore(Post)
    prefixedpost = createPostWithPrefix(underscoredpost)
    ontology.ontology_inserter.insertPersonPost(g, prefixedfiowithinitials,
                                                prefixedpost,
                                                "НГУ")
    return

for i in range(len(fios)):
    insertPersonInOntology(g, fios[i], posts[i], degs)

print("graph had %s statements before insertion" % before_insert)
print("graph has %s statements after insertion" % len(g))
ontology.ontology_inserter.saveOntology(g)



# ontology.ontology_inserter.insertPersonAndLabel(g, nepomPerson, "Непомнящий И.А")
# ontology.ontology_inserter.insertPersonName(g,nepomPerson, nepomPersonName)
# ontology.ontology_inserter.insertPersonLastName(g,nepomPerson, nepomPersonLastName)


# ontology.ontology_extractor.getPersonName(g, "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#СиваковаТВ")

#
# ontology.ontology_inserter.insertPersonPost(g, "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#НогинВД",
#                                             "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#профессор_должность",
#                                             "НГУ")
#
# ontology.ontology_inserter.insertPersonWorkPlace(g, "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#НогинВД",
#                                             "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#НГУ",
#                                             "НГУ")
#
# ontology.ontology_inserter.saveOntology(g)


# ls = ontology.ontology_extractor.getAllDegreeValues(g)
# print(deletePrefixFromList(ls))
# ls = ontology.ontology_extractor.getAllPhDValues(g)
# print(deletePrefixFromList(ls))
# ls = ontology.ontology_extractor.getAllPostValues(g)
# print(deletePrefixFromList(ls))


# zagorPerson = "http://www.semanticweb.org/IIS/ontologies/INIR_Ontology#ЗагорулькоЮА"

# print(ontology.ontology_extractor.getPersonInititals(g, zagorPerson))
# print(ontology.ontology_extractor.getPersonName(g, zagorPerson))
# print(ontology.ontology_extractor.getPersonLastName(g, zagorPerson))
# print(ontology.ontology_extractor.getPersonMiddleName(g, zagorPerson))

# print(ontology.ontology_inserter.deleteDotsInInitials(ontology.ontology_extractor.getPersonInititals(g, zagorPerson)))


